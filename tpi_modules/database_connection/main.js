var credentials = require('./tpi_modules/database/dbconfig.js');
console.log(credentials.url);

var getDBPromis = function(){
    console.log('Setze die Datenbank-URL auf ' + credentials.url);
    var pgp = require('pg-promise')();
    var db = pgp(credentials.url);
    return db;
};

fs = require('fs');
fs.readFile('./tpi_modules/database/tables/create_all_tables.sql', 'utf8', function(err, statement){
  if (err) {
    return console.log(err);
  } else{
    // console.log(statement);
  }
  var db = getDBPromis();
  db.any(statement).then(function(){
    console.log('success!');
  }).catch(function(err){
    console.log('error occured: ' + err);
  });
});
