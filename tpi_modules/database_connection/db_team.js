module.exports = function(){
  var db_team = {};
  var db_conn = require('./db_conn');
  var fs = require('fs');

  db_team.create_team = function(args, on_success, success_args, on_error, error_args){
    var statement = fs.readFileSync('./tpi_modules/database/team/create_team.sql', 'utf-8');
    db_conn.query(statement, args) // [user_id, team_name]
      .then(function(){on_success(success_args);})
      .catch(function(err){on_error(error_args.concat(err));});
  };

  db_team.add_team_user = function(args, on_success, success_args, on_error, error_args){
    var statement = fs.readFileSync('./tpi_modules/database/team/add_team_user.sql', 'utf-8');
    db_conn.query(statement, args) // [team_id, user_id]
      .then(function(){on_success(success_args);})
      .catch(function(err){on_error(error_args.concat(err));});
  };

  db_team.delete_team = function(args, on_success, success_args, on_error, error_args){
    var statement = fs.readFileSync('./tpi_modules/database/team/delete_team.sql', 'utf-8');
    db_conn.query(statement, args)
      .then(function(){on_success(success_args);})
      .catch(function(err){on_error(error_args.concat(err));});
  };

  db_team.update_team = function(args, on_success, success_args, on_error, error_args){
    var statement = fs.readFileSync('./tpi_modules/database/team/update_team.sql', 'utf-8');
    db_conn.query(statement, args)
      .then(function(){on_success(success_args);})
      .catch(function(err){on_error(error_args.concat(err));});
  };

  db_team.select_team = function(args, on_success, success_args, on_error, error_args){
    var statement = fs.readFileSync('./tpi_modules/database/team/select_team.sql', 'utf-8');
    db_conn.query(statement, args) // [team_id]
      .then(function(data){on_success(success_args.concat(data));})
      .catch(function(err){on_error(error_args.concat(err));});
  };

  db_team.select_teams_of_user = function(args, on_success, success_args, on_error, error_args){
    var statement = fs.readFileSync('./tpi_modules/database/team/select_teams_of_user.sql', 'utf-8');
    db_conn.query(statement, args) // [user_id]
      .then(function(data){on_success(success_args.concat([data]));})
      .catch(function(err){on_error(error_args.concat(err));});
  };

  return db_team;
}();
