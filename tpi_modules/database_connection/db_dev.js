module.exports = function(){
  var db_dev = {};
  var db_conn = require('./db_conn');
  var fs = require('fs');

  db_dev.drop_all = function(on_success, success_args, on_error, error_args){
    var statement = fs.readFileSync('./tpi_modules/database/tables/drop_all_tables.sql', 'utf-8');
    db_conn.query(statement)
      .then(function(){on_success(success_args);})
      .catch(function(err){on_error(error_args.concat(err));});
  };

  db_dev.create_all = function(on_success, success_args, on_error, error_args){
    var statement = fs.readFileSync('./tpi_modules/database/tables/create_all_tables.sql', 'utf-8');
    db_conn.query(statement)
      .then(function(){on_success(success_args);})
      .catch(function(err){on_error(error_args.concat(err));});
  };

  return db_dev;
}();
