module.exports = function(){
  var db_user = {};
  var db_conn = require('./db_conn');
  var fs = require('fs');

  db_user.create_user = function(args, on_success, success_args, on_error, error_args){
    var statement = fs.readFileSync('./tpi_modules/database/user/create_user.sql', 'utf-8');
    db_conn.query(statement, args)
      .then(function(){on_success(success_args);})
      .catch(function(err){on_error(error_args.concat(err));});
  };

  db_user.delete_user = function(args, on_success, success_args, on_error, error_args){
    var statement = fs.readFileSync('./tpi_modules/database/user/delete_user.sql', 'utf-8');
    db_conn.query(statement, args)
      .then(function(){on_success(success_args);})
      .catch(function(err){on_error(error_args.concat(err));});
  };

  db_user.update_user_names = function(args, on_success, success_args, on_error, error_args){
    var statement = fs.readFileSync('./tpi_modules/database/user/update_user_names.sql', 'utf-8');
    db_conn.query(statement, args)
      .then(function(){on_success(success_args);})
      .catch(function(err){on_error(error_args.concat(err));});
  };

  db_user.select_passwordhash = function(args, on_success, success_args, on_error, error_args){
    var statement = fs.readFileSync('./tpi_modules/database/user/select_passwordhash.sql', 'utf-8');
    db_conn.query(statement, args)
      .then(function(data){on_success(success_args.concat(data));})
      .catch(function(err){on_error(error_args.concat(err));});
  };

  db_user.create_token = function(args, on_success, success_args, on_error, error_args){
    var statement = fs.readFileSync('./tpi_modules/database/user/create_token.sql', 'utf-8');
    db_conn.query(statement, args)
      .then(function(){on_success(success_args);})
      .catch(function(err){on_error(error_args.concat(err));});
  };

  db_user.select_user_id_by_token = function(args, on_success, success_args, on_error, error_args){
    var statement = fs.readFileSync('./tpi_modules/database/user/select_user_id_by_token.sql', 'utf-8');
    db_conn.query(statement, args)
      .then(function(data){on_success(success_args.concat(data));})
      .catch(function(err){on_error(error_args.concat(err));});
  };

  db_user.delete_token = function(args, on_success, success_args, on_error, error_args){
    var statement = fs.readFileSync('./tpi_modules/database/user/delete_token.sql', 'utf-8');
    db_conn.query(statement, args)
      .then(function(){on_success(success_args);})
      .catch(function(err){on_error(error_args.concat(err));});
  };

  db_user.select_users = function(args, on_success, success_args, on_error, error_args){
    var statement = fs.readFileSync('./tpi_modules/database/user/select_users.sql', 'utf-8');
    db_conn.query(statement, args) // [user_id]
      .then(function(data){on_success(success_args.concat([data]));})
      .catch(function(err){on_error(error_args.concat(err));});
  };

  db_user.select_user = function(args, on_success, success_args, on_error, error_args){
    var statement = fs.readFileSync('./tpi_modules/database/user/select_user.sql', 'utf-8');
    db_conn.query(statement, args) // [user_id]
      .then(function(data){on_success(success_args.concat(data));})
      .catch(function(err){on_error(error_args.concat(err));});
  };

  db_user.select_users_of_event = function(args, on_success, success_args, on_error, error_args){
    var statement = fs.readFileSync('./tpi_modules/database/user/select_users_of_event.sql', 'utf-8');
    db_conn.query(statement, args) // [event_id]
      .then(function(data){on_success(success_args.concat([data]));})
      .catch(function(err){on_error(error_args.concat(err));});
  };

  db_user.select_users_of_team = function(args, on_success, success_args, on_error, error_args){
    var statement = fs.readFileSync('./tpi_modules/database/user/select_users_of_team.sql', 'utf-8');
    db_conn.query(statement, args) // [team_id]
      .then(function(data){on_success(success_args.concat([data]));})
      .catch(function(err){on_error(error_args.concat(err));});
  };

  db_user.select_users_not_in_team = function(args, on_success, success_args, on_error, error_args){
    var statement = fs.readFileSync('./tpi_modules/database/user/select_users_not_in_team.sql', 'utf-8');
    db_conn.query(statement, args) // [team_id]
      .then(function(data){on_success(success_args.concat([data]));})
      .catch(function(err){on_error(error_args.concat(err));});
  };

  return db_user;
}();
