module.exports = function(){
  var credentials = require('../database/dbconfig.js');
  var pgp = require('pg-promise')();
  // console.log(credentials.url);
  // console.log('Setze die Datenbank-URL auf ' + credentials.url);
  var db_conn = pgp(credentials.url);
  return db_conn;
}();
