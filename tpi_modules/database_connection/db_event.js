module.exports = function(){
  var db_event = {};
  var db_conn = require('./db_conn');
  var fs = require('fs');

  db_event.create_event = function(args, on_success, success_args, on_error, error_args){
    var statement = fs.readFileSync('./tpi_modules/database/event/create_event.sql', 'utf-8');
    db_conn.query(statement, args) // [team_id, name, start_date, start_time, end_date, end_time]
      .then(function(){on_success(success_args);})
      .catch(function(err){on_error(error_args.concat(err));});
  };

  db_event.delete_event = function(args, on_success, success_args, on_error, error_args){
    var statement = fs.readFileSync('./tpi_modules/database/event/delete_event.sql', 'utf-8');
    db_conn.query(statement, args)
      .then(function(){on_success(success_args);})
      .catch(function(err){on_error(error_args.concat(err));});
  };

  db_event.update_event = function(args, on_success, success_args, on_error, error_args){
    var statement = fs.readFileSync('./tpi_modules/database/event/update_event.sql', 'utf-8');
    db_conn.query(statement, args)
      .then(function(){on_success(success_args);})
      .catch(function(err){on_error(error_args.concat(err));});
  };

  db_event.select_event = function(args, on_success, success_args, on_error, error_args){
    var statement = fs.readFileSync('./tpi_modules/database/event/select_event.sql', 'utf-8');
    db_conn.query(statement, args) // [event_id]
      .then(function(data){on_success(success_args.concat(data));})
      .catch(function(err){on_error(error_args.concat(err));});
  };

  db_event.select_events_of_team = function(args, on_success, success_args, on_error, error_args){
    var statement = fs.readFileSync('./tpi_modules/database/event/select_events_of_team.sql', 'utf-8');
    db_conn.query(statement, args) // [team_id]
      .then(function(data){on_success(success_args.concat([data]));})
      .catch(function(err){on_error(error_args.concat(err));});
  };

  db_event.select_events_of_user = function(args, on_success, success_args, on_error, error_args){
    var statement = fs.readFileSync('./tpi_modules/database/event/select_events_of_user.sql', 'utf-8');
    db_conn.query(statement, args) // [user_id]
      .then(function(data){
        console.log("user_id: " + args[0]);
        console.log("Event-Data laoded from Database: " + JSON.stringify(data));
        on_success(success_args.concat([data]));
      })
      .catch(function(err){on_error(error_args.concat(err));});
  };

  db_event.accept = function(args, on_success, success_args, on_error, error_args){
    var statement = fs.readFileSync('./tpi_modules/database/event/accept_decline_undecide/accept.sql', 'utf-8');
    db_conn.query(statement, args) // [event_id, user_id]
      .then(function(){on_success(success_args.concat());})
      .catch(function(err){on_error(error_args.concat(err));});
  };

  db_event.decline = function(args, on_success, success_args, on_error, error_args){
    var statement = fs.readFileSync('./tpi_modules/database/event/accept_decline_undecide/decline.sql', 'utf-8');
    db_conn.query(statement, args) // [event_id, user_id]
      .then(function(){on_success(success_args.concat());})
      .catch(function(err){on_error(error_args.concat(err));});
  };

  db_event.undecide = function(args, on_success, success_args, on_error, error_args){
    var statement = fs.readFileSync('./tpi_modules/database/event/accept_decline_undecide/undecide.sql', 'utf-8');
    db_conn.query(statement, args) // [event_id, user_id]
      .then(function(){on_success(success_args.concat());})
      .catch(function(err){on_error(error_args.concat(err));});
  };

  return db_event;
}();
