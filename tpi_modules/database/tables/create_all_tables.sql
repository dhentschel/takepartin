BEGIN;
-- user
-- ======
CREATE SEQUENCE public.user_id_seq
  INCREMENT 1
  MINVALUE 1
  MAXVALUE 9223372036854775807
  START 1
  CACHE 1;
ALTER TABLE public.user_id_seq
  OWNER TO tpiadmin;

CREATE TABLE public."user"
(
  id integer NOT NULL DEFAULT nextval('user_id_seq'::regclass),
  username text NOT NULL UNIQUE,
  prename text,
  surname text,
  password text NOT NULL,
  CONSTRAINT user_pkey PRIMARY KEY (id)
)
WITH (
  OIDS=FALSE
);
ALTER TABLE public."user"
  OWNER TO tpiadmin;

-- team
-- ======
CREATE SEQUENCE public.team_id_seq
  INCREMENT 1
  MINVALUE 1
  MAXVALUE 9223372036854775807
  START 1
  CACHE 1;
ALTER TABLE public.team_id_seq
  OWNER TO tpiadmin;

CREATE TABLE public.team
(
  id integer NOT NULL DEFAULT nextval('team_id_seq'::regclass),
  name text NOT NULL,
  CONSTRAINT team_pkey PRIMARY KEY (id)
)
WITH (
  OIDS=FALSE
);
ALTER TABLE public.team
  OWNER TO tpiadmin;

-- team_user
-- ==========
CREATE TABLE public.team_user
(
  team_id integer NOT NULL,
  user_id integer NOT NULL,
  CONSTRAINT team_user_pkey PRIMARY KEY (team_id, user_id),
  CONSTRAINT team_user_team_id_fkey FOREIGN KEY (team_id)
      REFERENCES public.team (id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION,
  CONSTRAINT team_user_user_id_fkey FOREIGN KEY (user_id)
      REFERENCES public."user" (id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION
)
WITH (
  OIDS=FALSE
);
ALTER TABLE public.team_user
  OWNER TO tpiadmin;

-- event
-- ======
CREATE SEQUENCE public.event_id_seq
  INCREMENT 1
  MINVALUE 1
  MAXVALUE 9223372036854775807
  START 1
  CACHE 1;
ALTER TABLE public.event_id_seq
  OWNER TO tpiadmin;

CREATE TABLE public.event
(
  id integer NOT NULL DEFAULT nextval('event_id_seq'::regclass),
  team_id integer NOT NULL,
  name text NOT NULL,
  start_datetime timestamp with time zone NOT NULL,
  end_datetime timestamp with time zone NOT NULL,
  CONSTRAINT event_pkey PRIMARY KEY (id),
  CONSTRAINT event_team_id_fkey FOREIGN KEY (team_id)
      REFERENCES public.team (id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION
)
WITH (
  OIDS=FALSE
);
ALTER TABLE public.team_user
  OWNER TO tpiadmin;

-- event_user
-- ==========
CREATE TABLE public.event_user
(
  event_id integer NOT NULL,
  user_id integer NOT NULL,
  accepted boolean NOT NULL DEFAULT false,
  declined boolean NOT NULL DEFAULT false,
  undecided boolean NOT NULL DEFAULT true,
  CONSTRAINT event_user_pkey PRIMARY KEY (event_id, user_id),
  CONSTRAINT event_user_event_id_fkey FOREIGN KEY (event_id)
      REFERENCES public.event (id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION,
  CONSTRAINT event_user_user_id_fkey FOREIGN KEY (user_id)
      REFERENCES public."user" (id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION
)
WITH (
  OIDS=FALSE
);
ALTER TABLE public.event_user
  OWNER TO tpiadmin;

-- event_information
-- ==================
CREATE OR REPLACE VIEW public.event_information AS
 SELECT e.id,
    e.team_id,
    e.name,
    e.start_datetime,
    e.end_datetime,
    count(
        CASE
            WHEN eu.accepted THEN 1
            ELSE NULL::integer
        END) AS count_accepted,
    count(
        CASE
            WHEN eu.declined THEN 1
            ELSE NULL::integer
        END) AS count_declined,
    count(
        CASE
            WHEN eu.undecided THEN 1
            ELSE NULL::integer
        END) AS count_undecided
   FROM event e
     LEFT JOIN event_user eu ON e.id = eu.event_id
  GROUP BY eu.event_id, e.id;

ALTER TABLE public.event_information
  OWNER TO tpiadmin;

-- user_token
-- ===========
CREATE TABLE public.user_token
(
  user_id integer NOT NULL,
  token text NOT NULL,
  CONSTRAINT user_token_pkey PRIMARY KEY (user_id, token),
  CONSTRAINT user_token_user_id_fkey FOREIGN KEY (user_id)
      REFERENCES public."user" (id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION
)
WITH (
  OIDS=FALSE
);
ALTER TABLE public.user_token
  OWNER TO tpiadmin;


COMMIT;
