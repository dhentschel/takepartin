module.exports = function(){
  var paths = {
    create_event: './tpi_modules/database/event/create_event.sql',
    delete_event: './tpi_modules/database/event/delete_event.sql',
    update_event: './tpi_modules/database/event/update_event.sql',
    select_event: './tpi_modules/database/event/select_event.sql',
    select_events_of_team: './tpi_modules/database/event/select_events_of_team.sql',
    select_events_of_user: './tpi_modules/database/event/select_events_of_user.sql',
    accept: './tpi_modules/database/event/accept_decline_undecide/accept.sql',
    decline: './tpi_modules/database/event/accept_decline_undecide/decline.sql',
    undecide: './tpi_modules/database/event/accept_decline_undecide/undecide.sql',

    create_team: './tpi_modules/database/team/create_team.sql',
    add_team_user: './tpi_modules/database/team/add_team_user.sql',
    delete_team: './tpi_modules/database/team/delete_team.sql',
    update_team: './tpi_modules/database/team/update_team.sql',
    select_team: './tpi_modules/database/team/select_team.sql',
    select_teams_of_user: './tpi_modules/database/team/select_teams_of_user.sql',

    create_user: './tpi_modules/database/user/create_user.sql',
    delete_user: './tpi_modules/database/user/delete_user.sql',
    select_passwordhash: './tpi_modules/database/user/select_passwordhash.sql',
    create_token: './tpi_modules/database/user/create_token.sql',
    select_user_id_by_token: './tpi_modules/database/user/select_user_id_by_token.sql',
    delete_token: './tpi_modules/database/user/delete_token.sql',
    select_users: './tpi_modules/database/user/select_users.sql',
    select_user: './tpi_modules/database/user/select_user.sql',
    select_users_of_event: './tpi_modules/database/user/select_users_of_event.sql',
    select_users_of_team: './tpi_modules/database/user/select_users_of_team.sql',
    select_users_not_in_team: './tpi_modules/database/user/select_users_not_in_team.sql'
  };

  return paths;
}
