INSERT INTO public.event(team_id, name, start_datetime, end_datetime)
    VALUES ($1, $2, $3, $4);
INSERT INTO public.event_user(event_id, user_id)
    SELECT currval('event_id_seq') AS event_id, user_id
    FROM public.team_user
    WHERE public.team_user.team_id = $1;
