UPDATE public.event_user SET accepted=FALSE, declined=FALSE, undecided=TRUE WHERE event_id=$1 AND user_id=$2;
-- Logical Question: Should users be able to change their status after event has finished?
