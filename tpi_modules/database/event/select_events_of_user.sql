SELECT e.id,
  e.team_id,
  e.name,
  e.start_datetime,
  e.end_datetime,
  e.count_accepted,
  e.count_declined,
  e.count_undecided,
  eu.accepted,
  eu.declined,
  eu.undecided
  FROM public.event_information AS e
    INNER JOIN public.event_user AS eu
    ON e.id = eu.event_id
  WHERE eu.user_id=$1 AND e.end_datetime > now()
  ORDER BY start_datetime ASC;
