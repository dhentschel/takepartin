SELECT id,
  team_id,
  name,
  start_datetime,
  end_datetime,
  count_accepted,
  count_declined,
  count_undecided
  FROM public.event_information
  WHERE id=$1;
