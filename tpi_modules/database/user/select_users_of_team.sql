SELECT u.id, u.username, u.prename, u.surname
  FROM public.user AS u
  INNER JOIN public.team_user AS tu
    ON u.id=tu.user_id
  WHERE tu.team_id=$1
  ORDER BY u.username ASC;
