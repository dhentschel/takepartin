SELECT u.id, u.username, u.prename, u.surname
  FROM public.user AS u
  WHERE NOT EXISTS
    (SELECT 1
     FROM public.team_user AS tu
     WHERE tu.team_id = $1
	AND tu.user_id = u.id)
  ORDER BY u.username ASC;
