SELECT u.id, u.username, u.prename, u.surname, eu.accepted, eu.declined, eu.undecided
  FROM public.user AS u
  INNER JOIN public.event_user AS eu
    ON u.id=eu.user_id
  WHERE eu.event_id=$1
  ORDER BY eu.accepted DESC, eu.declined DESC, u.username ASC;
