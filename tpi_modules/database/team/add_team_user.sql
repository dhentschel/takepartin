INSERT INTO public.team_user(team_id, user_id) VALUES ($1, $2);
INSERT INTO public.event_user(event_id, user_id)
    SELECT id AS event_id, $2
    FROM public.event
    WHERE public.event.team_id = $1;
    -- Logical Question: Should users only be added to teams events when the events arnt finished yet?
