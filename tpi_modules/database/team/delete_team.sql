DELETE FROM public.event_user AS eu
 WHERE eu.event_id IN (SELECT e.id FROM public.team AS t INNER JOIN public.event AS e ON t.id = e.team_id WHERE t.id = $1);
DELETE FROM public.event AS e
 WHERE e.id IN (SELECT e.id FROM public.team AS t INNER JOIN public.event AS e ON t.id = e.team_id WHERE t.id = $1);
DELETE FROM public.team_user AS tu
  WHERE tu.team_id = $1;
DELETE FROM public.team AS t
  WHERE t.id = $1;
