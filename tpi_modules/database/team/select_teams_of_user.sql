SELECT t.id,
  t.name
  FROM public.team AS t
    INNER JOIN public.team_user AS tu
    ON t.id = tu.team_id
  WHERE tu.user_id=$1;
