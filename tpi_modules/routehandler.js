var express = require('express');

module.exports = function() {
//    var backendHelper = require("smartbackendHelper")(passport);
//    var oauth = backendHelper.oauthModel;

  var router = express.Router();
  var user = require('./logic/user');
  var event = require('./logic/event');
  var proxy = require('./logic/proxy');
  var team = require('./logic/team');
  router.use(user.set_user_id_from_token);
  // var tdg = require('./tabledatagateway.js');
  // var ts = require('./transactionscript.js');
  // var logger = require('./logger.js');
  // var validate = require('./validator.js');

  // Event requests
  router.post('/create_event', event.create_event);
  router.post('/delete_event', event.delete_event);
  router.post('/update_event', event.update_event);
  router.get('/event/:event_id', event.select_event);
  router.get('/events/:team_id/team', event.select_events_of_team);
  router.get('/events/:user_id/user', event.select_events_of_user);
  router.get('/events/user', [proxy.request_own_data, event.select_events_of_user]);
  router.put('/accept', event.accept);
  router.put('/decline', event.decline);
  router.put('/undecide', event.undecide);

  // Team requests
  router.post('/create_team', team.create_team);
  router.post('/add_team_user', team.add_team_user);
  router.post('/delete_team', team.delete_team);
  router.post('/update_team', team.update_team);
  router.get('/team/:team_id', team.select_team);
  router.get('/teams/:user_id/user', team.select_teams_of_user);
  router.get('/teams/user', [proxy.request_own_data, team.select_teams_of_user]);

  // useradministration
  router.post('/create_user', user.create_user);
  router.post('/delete_user', user.delete_user);
  router.post('/update_user_names', user.update_user_names);
  router.post('/login', user.login);
  router.post('/logout', user.logout);
  router.get('/users', user.select_users);
  router.get('/user/:user_id', user.select_user);
  router.get('/user', [proxy.request_own_data, user.select_user]);
  router.get('/users/:team_id/team', user.select_users_of_team);
  router.get('/users/:team_id/not_in_team', user.select_users_not_in_team);
  router.get('/users/:event_id/event', user.select_users_of_event);

  // only for testcases
  router.get('/test', helloWorld);
  router.get('/test/', helloWorld);

  // only for developer
  var developer = require('./logic/developer');

  router.get('/dev/drop_all', developer.drop_all);
  router.get('/dev/create_all', developer.create_all);

  function helloWorld(req, res) {
    res.status(200).send(JSON.stringify({success: true, text: "Hello World"}));
  }
  function notYetImplemented(req, res) {
    res.status(501).send('This function is not yet implemented.');
  }
  return router;
}
