module.exports = function(){
    var user = {};

    var passhash = require('../password-hash-and-salt-master');
    var basics = require('./basics.js');
    var db_user = require('../database_connection/db_user.js');

    user.set_user_id_from_token = function(req, res, next){
      // var username = req.headers['x-auth-token'].split(/\./)[0]; //isn't trusworthy cause it can be changed by user with dev tools

      if(req.path == "/login" || req.path == "/logout"|| req.path == "/create_user"){
        next();
      } else {
        if(!req.headers['x-auth-token']){
          basics.respond_unauthorized([res]);
        } else {
          var token = req.headers['x-auth-token'].split(/\./)[1];
          db_user.select_user_id_by_token(token, function(data){
            if (data.length == 0){
              basics.respond_unauthorized([res]);
            } else{
              req.headers.user_id = data[0].user_id;
              next();
            }
          }, [], basics.respond_server_error, [res]);
        }
      }
    };

    user.create_and_send_token = function(res, user_id){
      require('crypto').randomBytes(48, function(err, buffer) {
        var token = buffer.toString('hex');
        db_user.create_token([user_id, token], basics.respond_data_loaded, [res, token], basics.respond_server_error, [res]);
      });
    }

    user.create_user = function(req, res, next){
      var username = req.body.username;
      var password = req.body.password;
      var surname = req.body.surname;
      var prename = req.body.prename;

      passhash(password).hash(function(error, hash) {
        if(error){
          basics.respond_server_error([res, error]);
        }
        db_user.create_user([username, hash, surname, prename], basics.respond_executed, [res], basics.respond_server_error, [res]);
      });
    };

    user.delete_user = function(req, res, next){
      if(!req.headers.user_id){
        basics.respond_bad_request([res]);
      } else{
        var user_id = req.headers.user_id;
        db_user.delete_user(user_id, basics.respond_executed, [res], basics.respond_server_error, [res]);
      }
    };

    user.update_user_names = function(req, res, next){
      var args = [];
      if(!req.headers.user_id){
        basics.respond_bad_request([res]);
      } else{
        var user_id = req.headers.user_id;
        var surname = req.body.surname || "";
        var prename = req.body.prename || "";
        var args = [user_id, surname, prename];
        db_user.update_user_names(args, basics.respond_executed, [res], basics.respond_server_error, [res]);
      }
    };

    user.login = function(req, res, next){
      var username = req.body.username;
      var password = req.body.password;
      db_user.select_passwordhash(username, function(data){
        if(data.length == 0 || !data[0].password){
          basics.respond_not_found([res]);
        } else{
          var hash = data[0].password;
          var user_id = data[0].id;
          passhash(password).verifyAgainst(hash, function(error, verified) {
            if(error){
              basics.respond_server_error([res, error]);
            }
            if(!verified) {
              basics.respond_bad_request([res]);
            } else {
              user.create_and_send_token(res, user_id);
            }
          });
        }
      }, [], basics.respond_server_error, [res]);
    };

    user.logout = function(req, res, next){
      // var username = req.headers['x-auth-token'].split(/\./)[0]; //isn't trusworthy cause it can be changed by user with dev tools
      if(!req.headers['x-auth-token']){
        basics.respond_unauthorized([res]);
      } else {
        var token = req.headers['x-auth-token'].split(/\./)[1];
        db_user.delete_token(token, basics.respond_executed, [res], basics.respond_server_error, [res]);
      }
    };

    user.select_users = function(req, res, next){
      var args = []; // [user_id]
      db_user.select_users(args, basics.respond_data_loaded, [res], basics.respond_server_error, [res]);
    };

    user.select_user = function(req, res, next){
      var args = []; // [user_id]
      var user_id = req.headers.user_id;
      args = args.concat(user_id);
      db_user.select_user(args, basics.respond_data_loaded, [res], basics.respond_server_error, [res]);
    };

    user.select_users_of_event = function(req, res, next){
      var args = []; // [event_id]
      var event_id = req.params.event_id;
      args = args.concat(event_id);
      db_user.select_users_of_event(args, basics.respond_data_loaded, [res], basics.respond_server_error, [res]);
    };

    user.select_users_of_team = function(req, res, next){
      var args = []; // [team_id]
      var team_id = req.params.team_id;
      console.log(team_id);
      args = args.concat(team_id);
      db_user.select_users_of_team(args, basics.respond_data_loaded, [res], basics.respond_server_error, [res]);
    };

    user.select_users_not_in_team = function(req, res, next){
      var args = []; // [team_id]
      var team_id = req.params.team_id;
      args = args.concat(team_id);
      db_user.select_users_not_in_team(args, basics.respond_data_loaded, [res], basics.respond_server_error, [res]);
    };

    return user;
}();
