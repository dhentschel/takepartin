module.exports = function(){
    var team = {};

    var basics = require('./basics');
    var db_team = require('../database_connection/db_team');


    team.create_team = function(req, res, next){
      var args = []; // [user_id, team_name]
      var user_id = req.headers.user_id;
      var team_name = req.body.team_name;
      console.log(user_id + '  ' + team_name);
      args = args.concat(user_id).concat(team_name);
      db_team.create_team(args, basics.respond_executed, [res], basics.respond_server_error, [res]);
    };

    team.add_team_user = function(req, res, next){
      var args = []; // [team_id, user_id]
      var team_id = req.body.team_id;
      var user_id = req.body.user_id;
      args = args.concat(team_id).concat(user_id);
      db_team.add_team_user(args, basics.respond_executed, [res], basics.respond_server_error, [res]);
    };

    team.delete_team = function(req, res, next){
      var team_id = req.body.team_id;
      if(!team_id){
        basics.respond_server_error([res]);
      } else{
        db_team.delete_team(team_id, basics.respond_executed, [res], basics.respond_server_error, [res]);
      }
    };

    team.update_team = function(req, res, next){
      var team_id = req.body.team_id;
      var team_name = req.body.team_name; // Change team_name
      console.log(team_id + ' - ' + team_name);
      if(!team_id){
        basics.respond_server_error([res]);
      } else{
        db_team.update_team([team_id, team_name], basics.respond_executed, [res], basics.respond_server_error, [res]);
      }
    };

    team.select_team = function(req, res, next){
      var args = []; // [team_id]
      var team_id = req.params.team_id;
      args = args.concat(team_id);
      db_team.select_team(args, basics.respond_data_loaded, [res], basics.respond_server_error, [res]);
    };

    team.select_teams_of_user = function(req, res, next){
      var args = []; // [user_id]
      var user_id = req.params.user_id;
      args = args.concat(user_id);
      db_team.select_teams_of_user(args, basics.respond_data_loaded, [res], basics.respond_server_error, [res]);
    };

    return team;
}();
