module.exports = function(){
    var proxy = {};

    proxy.request_own_data = function(req, res, next){
      req.params.user_id = req.headers.user_id;
      next();
    };

    return proxy;
}();
