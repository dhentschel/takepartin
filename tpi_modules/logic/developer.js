module.exports = function(){
  var developer = {};

  var basics = require('./basics');
  var db_dev = require('../database_connection/db_dev');

  developer.drop_all = function(req, res, next){
    db_dev.drop_all(basics.respond_executed, [res], basics.respond_server_error, [res]);
  }

  developer.create_all = function(req, res, next){
    db_dev.create_all(basics.respond_executed, [res], basics.respond_server_error, [res]);
  }

  return developer;
}();
