module.exports = function(){
    var event = {};

    var basics = require('./basics');
    var db_event = require('../database_connection/db_event');


    event.create_event = function(req, res, next){
      var args = []; // [team_id, name, start_date, start_time, end_date, end_time]
      var team_id = req.body.team_id; var name = req.body.name;
      args = args.concat(team_id).concat(name);
      var start_datetime = req.body.start_datetime
      args = args.concat(start_datetime);
      var end_datetime = req.body.end_datetime;
      args = args.concat(end_datetime);
      db_event.create_event(args, basics.respond_executed, [res], basics.respond_server_error, [res]);
    };

    event.delete_event = function(req, res, next){
      var event_id = req.body.event_id;
      console.log(event_id);
      if(!event_id){
        basics.respond_server_error([res]);
      } else{
        db_event.delete_event(event_id, basics.respond_executed, [res], basics.respond_server_error, [res]);
      }
    };

    event.update_event = function(req, res, next){
      var event_id = req.body.event_id;
      var name = req.body.name;
      var start_datetime = req.body.start_datetime;
      var end_datetime = req.body.end_datetime;
      if(!event_id){
        basics.respond_server_error([res]);
      } else{
        db_event.update_event([event_id, name, start_datetime, end_datetime], basics.respond_executed, [res], basics.respond_server_error, [res]);
      }
    };

    event.select_event = function(req, res, next){
      var args = []; // [event_id]
      var event_id = req.params.event_id;
      args = args.concat(event_id);
      db_event.select_event(args, basics.respond_data_loaded, [res], basics.respond_server_error, [res]);
    };

    event.select_events_of_team = function(req, res, next){
      var args = []; // [team_id]
      var team_id = req.params.team_id;
      args = args.concat(team_id);
      db_event.select_events_of_team(args, basics.respond_data_loaded, [res], basics.respond_server_error, [res]);
    };

    event.select_events_of_user = function(req, res, next){
      var args = []; // [user_id]
      var user_id = req.params.user_id;
      args = args.concat(user_id);
      db_event.select_events_of_user(args, basics.respond_data_loaded, [res], basics.respond_server_error, [res]);
    };

    event.accept = function(req, res, next){
      var args = []; // [event_id, user_id]
      var event_id = req.body.event_id;
      var user_id = req.headers.user_id;
      args = args.concat(event_id).concat(user_id);
      db_event.accept(args, basics.respond_executed, [res], basics.respond_server_error, [res]);
    };

    event.decline = function(req, res, next){
      var args = []; // [event_id, user_id]
      var event_id = req.body.event_id;
      var user_id = req.headers.user_id;
      args = args.concat(event_id).concat(user_id);
      db_event.decline(args, basics.respond_executed, [res], basics.respond_server_error, [res]);
    };

    event.undecide = function(req, res, next){
      var args = []; // [event_id, user_id]
      var event_id = req.body.event_id;
      var user_id = req.headers.user_id;
      args = args.concat(event_id).concat(user_id);
      db_event.undecide(args, basics.respond_executed, [res], basics.respond_server_error, [res]);
    };


    return event;
}();
