module.exports = function(){
    var basics = {};

    basics.respond_data_loaded = function(args){
      var res = args[0];
      if(res == undefined) return;
      data = (args[1] == undefined ? [] : args[1]);
      res.status(200).send(data);
    };

    basics.respond_executed = function(args){
      var res = args[0];
      if(res == undefined) return;
      res.status(200).send();
    };

    basics.respond_data_created = function(args){
      var res = args[0];
      if(res == undefined) return;
      data = (args[1] == undefined ? [] : args[1]);
      res.status(201).send(data);
    };

    basics.respond_bad_request = function(args){
      var res = args[0];
      if(res == undefined) return;
      info = (args[1] == undefined ? "Bad Request" : args[1]);
      res.status(400).send(info);
    };

    basics.respond_unauthorized = function(args){
      var res = args[0];
      if(res == undefined) return;
      info = (args[1] == undefined ? "Unauthorized" : args[1]);
      res.status(401).send(info);
    };

    basics.respond_forbidden = function(args){
      var res = args[0];
      if(res == undefined) return;
      info = (args[1] == undefined ? "Forbidden" : args[1]);
      res.status(403).send(info);
    };

    basics.respond_not_found = function(args){
      var res = args[0];
      if(res == undefined) return;
      info = (args[1] == undefined ? "Not Found" : args[1]);
      res.status(404).send(info);
    };

    basics.respond_server_error = function(args){
      var res = args[0];
      if(res == undefined) return;
      var err = (args[1] == undefined ? "Server-Error" : args[1]);
      res.status(500).send(err);
    };

    return basics;
}();
