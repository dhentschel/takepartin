angular.module('starter.services', [])

.service('authservice',function($q, $http, apiendpoint){
  var LOCAL_TOKEN_KEY = 'tpi_usertoken';
  var username = '';
  var is_authenticated = false;
  var auth_token;

  var load_user_credentials = function(){
    var token = window.localStorage.getItem(LOCAL_TOKEN_KEY);
    if (token) {
      use_credentials(token);
    }
  };

  var store_user_token = function(token){
    // Keep me logged in
    window.localStorage.setItem(LOCAL_TOKEN_KEY, token);

    //Don't keep me logged in -> save to the session storage
    // ....

    use_credentials(token);
  };

  var use_credentials = function(token) {
    var username = token.split('.')[0];
    is_authenticated = true;
    auth_token = token;

    $http.defaults.headers.common['X-Auth-Token'] = token;
  };

  var destroy_user_credentials = function(resolve, reject){
    $http.post(apiendpoint.url + '/api/logout', {}).success(function(response) {
      auth_token = undefined;
      username = '';
      is_authenticated = false;
      $http.defaults.headers.common['X-Auth-Token'] = undefined;
      window.localStorage.removeItem(LOCAL_TOKEN_KEY);
      resolve('Logout success.');
    }).error(function(err){
      reject('logout failed.');
    });
  };

  var login = function(username, password){
    return $q(function(resolve, reject){
      var body = {
        username: username,
        password: password
      };
      $http.post(apiendpoint.url + '/api/login', body).success(function(response) {
        if(response){
          store_user_token(username + "." + response);
          resolve('Login success.');
        } else {
          console.log("Loin failed.");
          reject('login failed.');
        }
      }).error(function(err){
        console.log("Loin failed Error Catch.");
        reject('login failed.');
      });
    });
  };

  var logout = function(){
    return $q(function(resolve, reject){destroy_user_credentials(resolve, reject);});
  };

  load_user_credentials();

  return {
    login: login,
    logout: logout,
    is_authenticated: function() {return is_authenticated;},
    username: function(){return username;}
  };

})

.factory('authinterceptor',function($rootScope, $q, AUTH_EVENTS){
  return{
    responseError: function(response) {
      $rootScope.$broadcast({
        401: AUTH_EVENTS.notAuthenticated,
        403: AUTH_EVENTS.notAuthorized
      }[response.status],response);
      return $q.reject(response);
    }
  };
})

.config(function($httpProvider){
  $httpProvider.interceptors.push('authinterceptor');
})

.factory('apiendpoint', function($location, $ionicPopup) {
    var service={};
    if($location.$$host == undefined || $location.$$host == '' || $location.$$port == null){
        service.url = 'http://ec2-52-59-83-186.eu-central-1.compute.amazonaws.com:80';
    } else{
        service.url = 'http://' + $location.$$host + ':' + $location.$$port;
    }
//    service.url = "http://crowdinsurance.cloudf.de";

    return service;
})

.factory('formatter', function() {

  return {
    date: function(date) {
      return date.getDate() + "." + date.getMonth() + "." + date.getFullYear();
    },
    time: function(time) {
      var hours = time.getUTCHours() + "";
      if(hours.length == 1){
        hours = "0" + hours;
      }
      var minutes = time.getUTCMinutes() + "";
      if(minutes.length == 1){
        minutes = "0" + minutes;
      }
      return hours + ":" + minutes;
    },
    date_for_send: function(ISOString) {
      return ISOString.substring(0,10);
    },
    time_for_send: function(TimeString) {
      return TimeString.substring(0,8);
    }
  };
})

.factory('pass_event_data', function() {
  var event_data = {};
  var set_event_data = function(data){
    event_data = data;
  };
  var get_event_data = function(){
    return event_data;
  };
  return {
    set_event_data: set_event_data,
    get_event_data: get_event_data
  };
});

// appController.service('CacheHistoryReseter', ['$ionicHistory', function($ionicHistory){
//     this.reset = function () {
//         $ionicHistory.clearCache();
//         $ionicHistory.clearHistory();
//         $ionicHistory.nextViewOptions({ disableBack: true, historyRoot: true });
//     }
//
// }]);
