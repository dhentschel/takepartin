// Ionic Starter App

// angular.module is a global place for creating, registering and retrieving Angular modules
// 'starter' is the name of this angular module example (also set in a <body> attribute in index.html)
// the 2nd parameter is an array of 'requires'
// 'starter.services' is found in services.js
// 'starter.controllers' is found in controllers.js
angular.module('starter', ['ionic', 'starter.controllers', 'starter.services'])

.run(function($ionicPlatform) {
  $ionicPlatform.ready(function() {
    // Hide the accessory bar by default (remove this to show the accessory bar above the keyboard
    // for form inputs)
    if (window.cordova && window.cordova.plugins && window.cordova.plugins.Keyboard) {
      cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
      cordova.plugins.Keyboard.disableScroll(true);

    }
    if (window.StatusBar) {
      // org.apache.cordova.statusbar required
      StatusBar.styleDefault();
    }
  });
})

// .run(function($rootScope, $state, authservice, AUTH_EVENTS){
//   $rootScope.$on('$stateChangeStart', function(event, next, nextParams, fromState){
//     if(!authservice.is_authenticated()) {
//       if (next.name !== 'login' && next.name !== 'signup'){
//         event.preventDefault();
//         $state.go('login');
//       }
//     }
//   });
// })

.config(function($stateProvider, $urlRouterProvider, $httpProvider) {

  // Set header for user identification
  $httpProvider.defaults.headers.common.user_id = 1; //TODO: Replace Default, by dynamic

  //disable IE ajax request caching
  $httpProvider.defaults.headers.common['If-Modified-Since'] = 'Mon, 26 Jul 1997 05:00:00 GMT';
  // extra
  $httpProvider.defaults.headers.common['Cache-Control'] = 'no-cache';
  $httpProvider.defaults.headers.common['Pragma'] = 'no-cache';

  // Ionic uses AngularUI Router which uses the concept of states
  // Learn more here: https://github.com/angular-ui/ui-router
  // Set up the various states which the app can be in.
  // Each state's controller can be found in controllers.js
  $stateProvider

  .state('login', {
    url: '/login',
    templateUrl: 'modules/login/views/login.html',
    controller: 'LoginCtrl'
  })

  .state('signup', {
    url: '/signup',
    templateUrl: 'modules/login/views/signup.html',
    controller: 'SignUpCtrl'
  })
  // setup an abstract state for the tabs directive
  .state('tab', {
    url: '/tab',
    abstract: true,
    templateUrl: 'modules/tabs/views/tabs.html'
  })

  .state('tab.account', {
    url: '/account',
    views: {
      'tab-account': {
        templateUrl: 'modules/account/views/account.html',
        controller: 'AccountCtrl'
      }
    }
  })

    .state('tab.account_update', {
      url: '/account/update',
      views: {
        'tab-account': {
          templateUrl: 'modules/account/views/update_account.html',
          controller: 'UpdateAccountCtrl'
        }
      }
    })

  .state('tab.teams', {
    url: '/teams',
    views: {
      'tab-teams': {
        templateUrl: 'modules/teams/views/teams.html',
        controller: 'TeamsCtrl'
      }
    }
  })

    .state('tab.team_create_team', {
      url: '/team/create_team',
      views: {
        'tab-teams': {
          templateUrl: 'modules/teams/views/team_create_team.html',
          controller: 'TeamCreateTeamCtrl'
        }
      }
    })

    .state('tab.team_detail', {
      url: '/team/:team_id',
      views: {
        'tab-teams': {
          templateUrl: 'modules/teams/views/team_detail.html',
          controller: 'TeamDetailCtrl'
        }
      }
    })
      .state('tab.team_add_user', {
        url: '/team/:team_id/add_user',
        views: {
          'tab-teams': {
            templateUrl: 'modules/teams/views/team_add_user.html',
            controller: 'TeamAddUserCtrl'
          }
        }
      })
      .state('tab.team_create_event', {
        url: '/team/:team_id/create_event',
        views: {
          'tab-teams': {
            templateUrl: 'modules/teams/views/team_create_event.html',
            controller: 'TeamCreateEventCtrl'
          }
        }
      })
      .state('tab.team_update_team', {
        url: '/team/:team_id/update_team',
        views: {
          'tab-teams': {
            templateUrl: 'modules/teams/views/team_update_team.html',
            controller: 'TeamUpdateTeamCtrl'
          }
        }
      })

  .state('tab.events', {
    url: '/events',
    views: {
      'tab-events': {
        templateUrl: 'modules/events/views/events.html',
        controller: 'EventsCtrl'
      }
    }
  })
    .state('tab.event_detail', {
      url: '/event/:event_id',
      views: {
        'tab-events': {
          templateUrl: 'modules/events/views/event_detail.html',
          controller: 'EventDetailCtrl'
        }
      }
    })
      .state('tab.event_update_event', {
        url: '/event/:event_id/update_event',
        views: {
          'tab-events': {
            templateUrl: 'modules/events/views/event_update_event.html',
            controller: 'EventUpdateEventCtrl'
          }
        }
      })
      ;

  // if none of the above states are matched, use this as the fallback
  $urlRouterProvider.otherwise('/tab/events');

});
