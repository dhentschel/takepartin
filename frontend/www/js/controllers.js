var appController = angular.module('starter.controllers',  [])

.controller('AppCtrl', function($state, $scope, $ionicPopup, authservice, AUTH_EVENTS){
  $scope.username = authservice.username();

  $scope.set_current_username = function(name){
    $scope.username = name;
  }

  $scope.$on(AUTH_EVENTS.notAuthorized, function(event){
    var alertPopup = $ionicPopup.alert({
      title: 'Nicht authorisiert',
      template: 'Du kannst diese Daten nicht abrufen.'
    });
  });

  $scope.$on(AUTH_EVENTS.notAuthenticated, function(event){
    if ($state.current.name !== "login" && $state.current.name !== "signup"){
      authservice.logout();
      $state.go('login');
      var alertPopup = $ionicPopup.alert({
        title: 'Sitzung ist verloren gegangen',
        template: 'Sorry, du musst dich neu anmelden.'
      });
    }
  });
});
// All Controllers are stored in their corresponding modules
