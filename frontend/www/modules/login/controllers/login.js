appController.controller('LoginCtrl',function($scope, $state, $ionicPopup, apiendpoint, authservice){
  $scope.data = {};

  $scope.login = function(data){
    authservice.login(data.username, data.password).then(function(authenticated){
      $state.go('tab.events', {}, {reload: true});
      $scope.set_current_username(data.username);
    }, function(err) {
      var alertPopup = $ionicPopup.alert({
        title: 'Anmelden ist fehlgeschlagen',
        template: 'Bitte überprüfe deine Eingaben!'
      });
    })
  }

  $scope.go_to_signup = function(){
    $state.go('signup', {}, {reload: true});
    // $state.go('tab.events', {}, {reload: true});
  }
});
