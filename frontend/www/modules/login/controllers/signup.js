appController.controller('SignUpCtrl',function($scope, $state, $ionicPopup, $http, apiendpoint){

  $scope.new_user = {
    username : undefined,
    surname : undefined,
    prename : undefined,
    password : undefined,
    password_repeat : undefined
  };

  $scope.signup = function(data){
    var body = $scope.new_user;
    $http.post(apiendpoint.url + '/api/create_user', body).success(function(response) {
      var infoPopup = $ionicPopup.alert({
        title: 'Erfolgreich registriert!',
        template: 'Du kannst dich jetzt anmelden.'
      });
      $state.go('login');
    });
  };

  $scope.is_not_identical = function(){
    return !($scope.new_user.password === $scope.new_user.password_repeat);
  };



});
