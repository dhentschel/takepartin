appController.controller('TeamCreateTeamCtrl',function($scope, $http, $stateParams, $ionicPopup, $state, apiendpoint, formatter){

  $scope.new_team = {
    name : undefined
  };

  $scope.create_team = function(){
    var body = {
      team_name : $scope.new_team.name
    }
    $http.post(apiendpoint.url + '/api/create_team', body).success(function(response) {
      var infoPopup = $ionicPopup.alert({
        title: 'Team erfolgreich erstellt!',
        template: ''
      });
      $state.go('tab.teams');
    });
  };
});
