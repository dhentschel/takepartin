appController.controller('TeamsCtrl',function($scope, $http, $state, $ionicPopup, apiendpoint){

  $scope.update_teams = function(){
    $http.get(apiendpoint.url + '/api/teams/user').success(function(response) { //TODO: Dont use fix user
      $scope.teams = response;
    });
  }

  $scope.$on("$ionicView.beforeEnter", function(event, data){
    $scope.update_teams();
  });

  $scope.there_are_no_teams = true;
  $scope.$on("$ionicView.enter",function(event, data){
    if(!$scope.teams || $scope.teams.length === 0){
      $scope.there_are_no_teams = true;
    } else{
      $scope.there_are_no_teams = false;
    }
  });

  $scope.go_to_create_team = function(){
    $state.go('tab.team_create_team');
  };

  $scope.go_to_team_detail = function(team_id){
    $state.go('tab.team_detail',{team_id: team_id});
  };


});
