appController.controller('TeamCreateEventCtrl',function($scope, $http, $stateParams, $state, $ionicPopup, apiendpoint, formatter){

  $scope.new_event = {
    name : undefined,
    start_date : undefined,
    start_time : undefined,
    end_date : undefined,
    end_time : undefined
  };

  $scope.create_event = function(){
    var body = {
      team_id : $stateParams.team_id,
      name : $scope.new_event.name,
      start_datetime : $scope.new_event.start_datetime.toISOString(),
      end_datetime : $scope.new_event.end_datetime.toISOString()
    }
    $http.post(apiendpoint.url + '/api/create_event', body).success(function(response) {
      var infoPopup = $ionicPopup.alert({
        title: 'Event erfolgreich erstellt!',
        template: ''
      });
      $state.go('tab.events');
    });
  };
});
