appController.controller('TeamAddUserCtrl',function($scope, $http, $stateParams, $ionicPopup, $state, apiendpoint){

  $scope.data = {
    id : undefined
  };

  $scope.user_choice_changed = function(chosen_user){
    $scope.user = chosen_user;
  }

  $scope.update_team_add_user = function(){
    $http.get(apiendpoint.url + '/api/users/' + $stateParams.team_id + '/not_in_team').success(function(response) {
      $scope.users = response;
    });
  }

  $scope.add_user_to_team = function(){
    var team_user = {
      team_id : $stateParams.team_id,
      user_id : $scope.user.id
    };
    $http.post(apiendpoint.url + '/api/add_team_user', team_user).success(function(response) {
      var infoPopup = $ionicPopup.alert({
        title: 'Erfolgreich hinzugefügt!',
        template: ''
      });
      $state.go('tab.team_detail',{team_id: $stateParams.team_id});
    });
  };

  $scope.$on("$ionicView.beforeEnter", function(event, data){
    $scope.update_team_add_user();
  });
});
