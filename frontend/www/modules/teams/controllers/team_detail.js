appController.controller('TeamDetailCtrl',function($scope, $http, $stateParams, $state, $ionicPopup, apiendpoint, formatter){

  $scope.update_team_detail = function(){
    // $http.get(apiendpoint.url + '/api/events/' + $stateParams.team_id + '/team').success(function(response) {
    //   for(var i=0; i<response.length; i++){
    //     response[i].start_date = formatter.date(response[i].start_date);
    //     response[i].end_date = formatter.date(response[i].end_date);
    //     if(response[i].start_date == response[i].end_date){
    //       response[i].end_date = "";
    //     }
    //   }
    //   $scope.events = response;
    // });
    $http.get(apiendpoint.url + '/api/team/' + $stateParams.team_id).success(function(response) {
      $scope.team = response;
    });
    $http.get(apiendpoint.url + '/api/users/' + $stateParams.team_id + '/team').success(function(response) {
      $scope.users = response;
    });
  };

  $scope.go_to_create_event = function(){
      $state.go('tab.team_create_event',{team_id: $scope.team.id});
  };

  $scope.go_to_add_user = function(){
      $state.go('tab.team_add_user',{team_id: $scope.team.id});
  };

  $scope.go_to_update_team = function(){
      $state.go('tab.team_update_team',{team_id: $scope.team.id});
  };

  $scope.go_to_add_user = function(){
      $state.go('tab.team_add_user',{team_id: $scope.team.id});
  };

  $scope.delete_team = function(){
    if(!$stateParams.team_id){
      return;
    }
    var confirmPopup = $ionicPopup.confirm({
      title: 'Team löschen',
      template: 'Bist du dir sicher, dass du dieses Team löschen willst?'
    });
    confirmPopup.then(function(res) {
      if(res) {
        var body = {
          team_id : $stateParams.team_id
        };
        $http.post(apiendpoint.url + '/api/delete_team', body).success(function(response) {
          var infoPopup = $ionicPopup.alert({
            title: 'Team erfolgreich gelöscht!',
            template: ''
          });
          $state.go('tab.teams');
        });
      } else {
        console.log('Abort delete team.');
      }
    });
  };

  $scope.$on("$ionicView.beforeEnter", function(event, data){
    $scope.update_team_detail();
  });

  // ============================
  // Popup for creating an event
  // ============================
  // $scope.show_popup_create_event = function() {
  //  $scope.new_event = {}
  //
  //  // An elaborate, custom popup
  //  var new_event_popup = $ionicPopup.show({
  //    template: 'Name: <input type="text" ng-model="new_event.event_name">',
  //    title: 'Create new Event',
  //    subTitle: 'Please enter your events information',
  //    scope: $scope,
  //    buttons: [
  //      {
  //        text: 'Cancel',
  //        type: 'button-negative',
  //        onTab: function(event){
  //          return "Canceled creation of a event";
  //        }
  //      },
  //      {
  //        text: '<b>Create</b>',
  //        type: 'button-positive',
  //        onTap: function(event) {
  //           if (!$scope.new_event.event_name) {
  //             //don't allow the user to close unless he enters wifi password
  //             event.preventDefault();
  //           } else {
  //             $http.post(apiendpoint.url + '/api/create_event', $scope.new_event).success(function(response) {
  //                 // CacheHistoryReseter.reset();
  //                 $scope.update_teams();
  //                 // $state.go('app.versicherungAdded',{id: response[0].createversicherung});
  //                 console.log("Successfully created the team.")
  //             });
  //             return $scope.new_event.name;
  //           }
  //        }
  //      },
  //    ]
  //  });
  //  new_team_popup.then(function(res) {
  //    console.log('Response', res);
  //  });
  // };
  //
  // // ==========================
  // // Popup for adding user
  // // ==========================
  // $scope.update_add_user_to_team_popup = function(){
  //   $http.get(apiendpoint.url + '/api/users').success(function(response) {
  //     $scope.new_users = response;
  //   });
  // }
  //
  // $scope.show_popup_add_user_to_team = function(){
  //   $scope.update_add_user_to_team_popup();
  //   $scope.data = {id : "ng"};
  //   $scope.user_choice_changed = function(chosen_user){
  //     $scope.new_user = chosen_user;
  //   }
  //
  //   // An elaborate, custom popup
  //   var add_user_to_team_popup = $ionicPopup.show({
  //     //template: '<ion-radio ng-repeat="chosen_user in new_users" ng-value="chosen_user.id" ng-model="data.id" ng-change="user_choice_changed(chosen_user)" name="choose_user">{{ chosen_user.name }}</ion-radio>',
  //     templateUrl: 'popup_add_user_to_team.html',
  //     title: 'Add new team members',
  //     subTitle: 'Please select a user you want to add',
  //     scope: $scope,
  //     buttons: [
  //       {
  //         text: 'Cancel',
  //         type: 'button-negative',
  //         onTab: function(event){
  //           return "Canceled creation of a team";
  //         }
  //       },
  //       {
  //         text: '<b>Add</b>',
  //         type: 'button-positive',
  //         onTap: function(event) {
  //            if (!$scope.new_user) {
  //              event.preventDefault();
  //            } else {
  //              var team_user = {
  //                team_id : $stateParams.team_id,
  //                user_id : $scope.new_user.id
  //              }
  //              $http.post(apiendpoint.url + '/api/add_team_user', team_user).success(function(response) {
  //                  $scope.update_teams();
  //                  console.log("Successfully added " + team_user.user_id + " to the team.")
  //              });
  //              return $scope.new_user;
  //            }
  //         }
  //       },
  //     ]
  //   });
  //   add_user_to_team_popup.then(function(res) {
  //     console.log('Response ', res);
  //     $scope.update_team_detail();
  //   });
  // }
});
