appController.controller('TeamUpdateTeamCtrl',function($scope, $http, $stateParams, $state, $ionicPopup, apiendpoint, formatter){

  $scope.team = {
    team_name : undefined
  };

  $scope.update_team = function(){
    var body = {
      team_id : $stateParams.team_id,
      team_name : $scope.team.team_name
    }
    $http.post(apiendpoint.url + '/api/update_team', body).success(function(response) {
      var infoPopup = $ionicPopup.alert({
        title: 'Team erfolgreich geändert!',
        template: ''
      });
      $state.go('tab.team_detail',{team_id: $stateParams.team_id});
    });
  };
});
