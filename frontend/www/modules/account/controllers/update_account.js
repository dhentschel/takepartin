appController.controller('UpdateAccountCtrl',function($scope, $state, $ionicPopup, $http, apiendpoint){

  $scope.update_names_user = {
    surname : undefined,
    prename : undefined
  };

  $scope.prefill_names = function(){
    $http.get(apiendpoint.url + '/api/user').success(function(response) {
      $scope.update_names_user.surname = response.surname;
      $scope.update_names_user.prename = response.prename;
    });
  };

  $scope.$on("$ionicView.beforeEnter", function(event, data){
    $scope.prefill_names();
  });

  $scope.update_names = function(){
    var body = $scope.update_names_user;
    $http.post(apiendpoint.url + '/api/update_user_names', body).success(function(response) {
      var infoPopup = $ionicPopup.alert({
        title: 'Namen erfolgreich geändert!',
        template: ''
      });
      $state.go('tab.account');
    });
  };

  // $scope.is_not_identical = function(){
  //   return !($scope.new_user.password === $scope.new_user.password_repeat);
  // };



});
