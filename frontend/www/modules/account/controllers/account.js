appController.controller('AccountCtrl',function($scope, $http, $state, $ionicPopup, authservice, apiendpoint){

  $scope.update_account = function(){
    $http.get(apiendpoint.url + '/api/user').success(function(response) {
      $scope.user = response;
    });
  };

  $scope.logout = function(){
    authservice.logout().then(function(authenticated){
      $state.go('login');
      $scope.set_current_username('');
    }, function(err) {
      var alertPopup = $ionicPopup.alert({
        title: 'Abmeldung ist fehlgeschlagen',
        template: 'Unbekannter Fehler'
      });
    });
  };

  $scope.go_to_update_account = function(){
    $state.go('tab.account_update');
  };

  $scope.delete_user = function(){
    var confirmPopup = $ionicPopup.confirm({
      title: 'Benutzer löschen',
      template: 'Bist du dir sicher, dass du deinen Benutzer löschen möchtest?'
    });
    confirmPopup.then(function(res) {
      if(res) {
        $http.post(apiendpoint.url + '/api/delete_user', {}).success(function(response) {
          var infoPopup = $ionicPopup.alert({
            title: 'Konto erfolgreich gelöscht!',
            template: ''
          });
          $state.go('login');
        });
      } else {
      }
    });
  };

  $scope.$on("$ionicView.beforeEnter", function(event, data){
    $scope.update_account();
    if($http.defaults.headers.common.user_id != undefined){
      $scope.data = {id : $http.defaults.headers.common.user_id};
    } else{$scope.data = {id : 1}};
  });
});
