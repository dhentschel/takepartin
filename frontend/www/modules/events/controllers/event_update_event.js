appController.controller('EventUpdateEventCtrl',function($scope, $http, $stateParams, $state, $ionicPopup, apiendpoint, formatter, pass_event_data){

  $scope.event = {
    name : undefined,
    start_date : undefined,
    start_time : undefined,
    end_date : undefined,
    end_time : undefined
  };

  $scope.$on('$ionicView.enter', function(){
    var data = pass_event_data.get_event_data();
    var start_datetime = data.start_datetime;
    var end_datetime = data.end_datetime;
    $scope.event = {
      name : data.name,
      start_datetime : new Date(start_datetime),
      end_datetime : new Date(end_datetime)
    };
  })

  $scope.update_event = function(){
    var body = {
      event_id : $stateParams.event_id,
      name : $scope.event.name,
      start_datetime : $scope.event.start_datetime.toISOString(),
      end_datetime : $scope.event.end_datetime.toISOString()
    }
    $http.post(apiendpoint.url + '/api/update_event', body).success(function(response) {
      var infoPopup = $ionicPopup.alert({
        title: 'Event erfolgreich geändert!',
        template: ''
      });
      $state.go('tab.event_detail',{event_id: $stateParams.event_id});
    });
  };
});
