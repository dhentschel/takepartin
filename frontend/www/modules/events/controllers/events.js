appController.controller('EventsCtrl',function($scope, $http, $state, apiendpoint, formatter){

  $scope.update_events = function(){
    $http.get(apiendpoint.url + '/api/events/user').success(function(response) {
      for(var i=0; i<response.length; i++){
        var start = new Date(response[i].start_datetime);
        var end = new Date(response[i].end_datetime);
        response[i].start_date = start.toLocaleDateString();
        response[i].end_date = end.toLocaleDateString();
        response[i].start_time = start.toLocaleTimeString();
        response[i].end_time = end.toLocaleTimeString();
        if(response[i].start_date == response[i].end_date){
          response[i].end_date = "";
        }
      }
      $scope.events = response;
    });
  };

  $scope.get_item_class = function(event){
      if(event.accepted == true){
        return "item-remove-animate item-avatar item-icon-right item-balanced";
      }
      if(event.declined == true){
        return "item-remove-animate item-avatar item-icon-right item-assertive";
      }
      if(event.undecided == true){
        return "item-remove-animate item-avatar item-icon-right item-stable";
      }
  };

  $scope.go_to_event_detail = function(event_id){
    $state.go('tab.event_detail',{event_id: event_id});
  };

  $scope.accept_event = function(event_id){
    event.stopImmediatePropagation();
    var body = {event_id :event_id};
    $http.put(apiendpoint.url + '/api/accept', body).success(function(response) {
      $scope.update_events();
    });
  };

  $scope.decline_event = function(event_id){
    event.stopImmediatePropagation();
    var body = {event_id :event_id};
    $http.put(apiendpoint.url + '/api/decline', body).success(function(response) {
      $scope.update_events();
    });
  };

  $scope.undecide_event = function(event_id){
    event.stopImmediatePropagation();
    var body = {event_id :event_id};
    $http.put(apiendpoint.url + '/api/undecide', body).success(function(response) {
      $scope.update_events();
    });
  };



  $scope.$on("$ionicView.beforeEnter", function(event, data){
    $scope.update_events();
  });

  $scope.there_are_no_events = true;
  $scope.$on("$ionicView.enter",function(event, data){
    if(!$scope.events || $scope.events.length === 0){
      $scope.there_are_no_events = true;
    } else{
      $scope.there_are_no_events = false;
    }
  });

});
