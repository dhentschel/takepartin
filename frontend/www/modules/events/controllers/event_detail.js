appController.controller('EventDetailCtrl',function($scope, $http, $stateParams, $state, $ionicPopup, apiendpoint, pass_event_data){

  $scope.update_event_detail = function(){
    $http.get(apiendpoint.url + '/api/event/' + $stateParams.event_id).success(function(response) { //TODO: Create interface for Team_data
      $scope.event = response;
    });
    $http.get(apiendpoint.url + '/api/users/' + $stateParams.event_id + '/event').success(function(response) {
      $scope.users = response;
    });
  };

  $scope.go_to_update_event = function(){
      pass_event_data.set_event_data($scope.event);
      $state.go('tab.event_update_event',{event_id: $stateParams.event_id});
  };

  $scope.get_item_class = function(user){
    if(user.accepted == true){
      return "item-remove-animate item-avatar item-balanced";
    }
    if(user.declined == true){
      return "item-remove-animate item-avatar item-assertive";
    }
    if(user.undecided == true){
      return "item-remove-animate item-avatar item-stable";
    }
  };

  $scope.delete_event = function(){
    if(!$stateParams.event_id){
      return;
    }
    var confirmPopup = $ionicPopup.confirm({
      title: 'Event löschen',
      template: 'Bist du dir sicher, dass du dieses Event löschen möchtest?'
     });
     confirmPopup.then(function(res) {
      if(res) {
        var body = {
          event_id : $stateParams.event_id
        };
        $http.post(apiendpoint.url + '/api/delete_event', body).success(function(response) {
          var infoPopup = $ionicPopup.alert({
            title: 'Event erfolgreich gelöscht!',
            template: ''
          });
          $state.go('tab.events');
        });
      } else {
        // console.log('Canceled deleting event');
      }
    });
  };

  $scope.$on("$ionicView.beforeEnter", function(event, data){
    $scope.update_event_detail();
  });
});
